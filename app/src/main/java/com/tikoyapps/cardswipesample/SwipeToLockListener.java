package com.tikoyapps.cardswipesample;

import android.app.Fragment;
import android.content.Context;

import com.daimajia.swipe.SwipeLayout;

/**
 * Created by xcptan on 7/13/15.
 */
public class SwipeToLockListener implements SwipeLayout.SwipeListener {

    interface updateCallback{
        void setLocked(boolean locked);
    }

    Fragment mFragment;

    public SwipeToLockListener(Fragment fragment) {
        mFragment = fragment;
    }

    @Override
    public void onStartOpen(SwipeLayout layout) {
        
    }

    @Override
    public void onOpen(SwipeLayout layout) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((CardFragment)mFragment).setLocked(true);
    }

    @Override
    public void onStartClose(SwipeLayout layout) {

    }

    @Override
    public void onClose(SwipeLayout layout) {

    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

    }
}
