package com.tikoyapps.cardswipesample;



import android.app.Fragment;

import com.daimajia.swipe.SwipeLayout;

/**
 * Created by xcptan on 7/13/15.
 */
public class SwipeToUnlockListener implements SwipeLayout.SwipeListener {

    Fragment mFragment;

    interface updateCallback{
        void setLocked(boolean locked);
    }

    public SwipeToUnlockListener(Fragment fragment) {
        this.mFragment = fragment;
    }

    @Override
    public void onStartOpen(SwipeLayout layout) {

    }

    @Override
    public void onOpen(SwipeLayout layout) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((CardFragment)mFragment).setLocked(false);
    }

    @Override
    public void onStartClose(SwipeLayout layout) {

    }

    @Override
    public void onClose(SwipeLayout layout) {

    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

    }
}
