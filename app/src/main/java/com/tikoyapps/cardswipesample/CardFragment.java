package com.tikoyapps.cardswipesample;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by xcptan on 7/13/15.
 */
public class CardFragment extends Fragment implements SwipeToLockListener.updateCallback,
        SwipeToUnlockListener.updateCallback {

    boolean locked = false;

    /**
     * Card components
     */
    @InjectView(R.id.lbm_swipelayout_carditem_parent)
    ViewGroup swipeLayoutParent;
    @InjectView(R.id.lbm_swipelayout_carditem_cardalias_textview)
    TextView cardAliasTextView;
    @InjectView(R.id.lbm_swipelayout_carditem_swipetounlock_parent_swipelayout)
    SwipeLayout swipeLayoutUnlock;
    @InjectView(R.id.lbm_swipelayout_carditem_swipetolock_parent_swipelayout)
    SwipeLayout swipeLayoutLock;
    @InjectView(R.id.lbm_swipelayout_carditem_institutionlogo_imageview)
    ImageView cardImageView;
    @InjectView(R.id.lbm_swipelayout_carditem_numbermask_textview)
    TextView numberMask;
    @InjectView(R.id.lbm_swipelayout_carditem_lastfourdigits_textview)
    TextView lastFourDigits;
    @InjectView(R.id.lbm_swipelayout_carditem_channelsettings_textview)
    TextView cardSettings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        View root = inflater.inflate(R.layout.carditem_layout, container, false);
        ButterKnife.inject(this, root);

        initializeCardDetails();

        return root;
    }

    private void initializeCardDetails() {
        Picasso.with(getActivity()).load("https://upload.wikimedia" +
                ".org/wikipedia/commons/thumb/b/b7/MasterCard_Logo.svg/1280px-MasterCard_Logo.svg" +
                ".png").into(cardImageView);
        cardAliasTextView.setText("My Card");
        lastFourDigits.setText("1234");
        cardSettings.setText("Sample Data");

        if (locked) {
            swipeLayoutLock.setVisibility(View.GONE);
            swipeLayoutUnlock.setVisibility(View.VISIBLE);
            swipeLayoutUnlock.close();
        } else {
            swipeLayoutLock.close();
            swipeLayoutUnlock.setVisibility(View.GONE);
            swipeLayoutLock.setVisibility(View.VISIBLE);
        }

        swipeLayoutUnlock.setShowMode(SwipeLayout.ShowMode.LayDown);
        swipeLayoutUnlock.setDragEdge(SwipeLayout.DragEdge.Left);

        swipeLayoutUnlock.removeAllSwipeListener();
        swipeLayoutUnlock.addSwipeListener(new SwipeToUnlockListener(this));

        swipeLayoutLock.setShowMode(SwipeLayout.ShowMode.LayDown);
        swipeLayoutLock.setDragEdge(SwipeLayout.DragEdge.Left);

        swipeLayoutLock.removeAllSwipeListener();
        swipeLayoutLock.addSwipeListener(new SwipeToLockListener(this));
    }

    @Override
    public void setLocked(boolean locked) {
        this.locked = locked;
        initializeCardDetails();
    }
}
