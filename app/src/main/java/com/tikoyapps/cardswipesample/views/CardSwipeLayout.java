package com.tikoyapps.cardswipesample.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by xytan on 6/26/15.
 */

/**
 * Layout that pre computes height depending on width
 * to follow a real world credit card ratio of
 * 1:0.631 Width:Height
 */

public class CardSwipeLayout extends RelativeLayout {
    public CardSwipeLayout(Context context) {
        super(context);
    }

    public CardSwipeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardSwipeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = Math.round(widthSize * 0.631f);

        setMeasuredDimension(widthSize, heightSize);

        int newHeightMeasureSpec = MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, newHeightMeasureSpec);
    }
}
