package com.tikoyapps.cardswipesample.views;

import android.content.Context;
import android.graphics.Typeface;

final class FontManager {

    private static Typeface robotoRegularTypeface;

    private static Typeface robotoBoldTypeface;

    private static Typeface robotoBlackTypeface;

    private static Typeface robotoLightTypeface;

    private static Typeface robotoThinTypeface;

    private FontManager() {
    }

    public static Typeface getLightFont(Context context) {
        if (robotoLightTypeface == null) {
            robotoLightTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        }
        return robotoRegularTypeface;
    }

    public static Typeface getDefaultFont(Context context) {
        if (robotoRegularTypeface == null) {
            robotoRegularTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        }
        return robotoRegularTypeface;
    }

    public static Typeface getBoldFont(Context context) {
        if (robotoBoldTypeface == null) {
            robotoBoldTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        }
        return robotoBoldTypeface;
    }

    public static Typeface getExtraBoldFont(Context context) {
        if (robotoBlackTypeface == null) {
            robotoBlackTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
        }
        return robotoBlackTypeface;
    }

    public static Typeface getThinFont(Context context) {
        if (robotoThinTypeface == null) {
            robotoThinTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
        }
        return robotoThinTypeface;
    }

}
