package com.tikoyapps.cardswipesample.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.tikoyapps.cardswipesample.R;

/**
 * Created by xcptan on 7/1/15.
 */
public class RobotoTextView extends AppCompatTextView {
    private int defaultDimension = 0;

    private int ROBOTO_REGULAR = 1;
    private int ROBOTO_THIN = 2;
    private int ROBOTO_LIGHT = 3;

    private int BOLD = 1;
    private int ITALIC = 2;

    private int fontNameId = 0;
    private int fontStyleId = 0;


    public RobotoTextView(Context context) {
        super(context);
        init(null, 0);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.font, defStyle, 0);
        fontNameId = a.getInt(R.styleable.font_name, defaultDimension);
        fontStyleId = a.getInt(R.styleable.font_style, defaultDimension);
        a.recycle();
        if (fontNameId == ROBOTO_REGULAR) {
            setFontType(FontManager.getDefaultFont(getContext()));
        } else if (fontNameId == ROBOTO_THIN) {
            setFontType(FontManager.getThinFont(getContext()));
        } else if (fontNameId == ROBOTO_LIGHT) {
            setFontType(FontManager.getLightFont(getContext()));
        } else {
            setFontType(FontManager.getDefaultFont(getContext()));
        }
    }

    private void setFontType(Typeface font) {
        if (fontStyleId == BOLD) {
            setTypeface(font, Typeface.BOLD);
        } else if (fontStyleId == ITALIC) {
            setTypeface(font, Typeface.ITALIC);
        } else {
            setTypeface(font);
        }
    }
}
